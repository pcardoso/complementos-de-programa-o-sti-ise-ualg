import flask
from flask import request, jsonify, g
import sqlite3

DATABASE = 'sensors.db'

app = flask.Flask(__name__)


def get_db():
    """ Opens a new database connection if there is none yet for the current application context.
    In Flask, g is a global object that is used to store data during a single request.
    It is designed to be used for storing data that should be accessible globally within the context of a request
    but should not be shared between different requests."""
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    """ This function is called automatically by Flask when the application context is torn down.
    The purpose of this teardown function is to clean up resources associated with the application context,
    specifically here, to close the database connection."""
    db = getattr(g, '_database', None)
    print("--", db)
    if db is not None:
        db.close()
        print(exception)


def fetchall_dict(cursor):
    """Fetch all rows from the cursor and return them as dictionaries."""
    rows = cursor.fetchall()
    columns = [column[0] for column in cursor.description]
    return [dict(zip(columns, row)) for row in rows]


@app.route('/iot/api/v1/readings', methods=['GET'])
def list_readings():
    sql = """SELECT * 
             FROM Reading 
                INNER JOIN Sensor on Reading.idSensor = Sensor.idSensor
                INNER JOIN Location on Sensor.idLocation = Location.idLocation"""
    cur = get_db().cursor()
    cur.execute(sql)
    readings = fetchall_dict(cur)
    return jsonify(readings), 200


@app.route('/iot/api/v1/readings/<int:id>', methods=['GET'])
def list_reading(id):
    sql = """SELECT * 
             FROM Reading 
                INNER JOIN Sensor on Reading.idSensor = Sensor.idSensor
                INNER JOIN Location on Sensor.idLocation = Location.idLocation
                WHERE Reading.idReading = ?"""
    cur = get_db().cursor()
    cur.execute(sql, (id,))
    readings = fetchall_dict(cur)
    return jsonify(readings), 200

if __name__ == '__main__':
    app.run(debug=True)

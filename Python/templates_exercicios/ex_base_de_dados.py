import mysql.connector
from pprint import pprint

config = {
    'host' : 'localhost',
    'user' : 'adam',
    'password' : 'adam',
    'db' : 'adamastor' # notem que no phpmyadmin dei permissões ao utilizador sensors para ter acesso à base de dados adamastor
}


class Adamastor:
    def __init__(self, config):
        self.cnx = mysql.connector.connect(**config)
        self.cursor = self.cnx.cursor(dictionary=True)
    
    def listar_clientes(self):
        sql = 'SELECT * FROM clientes'
        self.cursor.execute(sql)
        return self.cursor.fetchall()
        
    def listar_clientes_da_cidade(self, cidade):
        sql = 'SELECT * FROM clientes where cidade = %s'
        self.cursor.execute(sql, (cidade, ))
        return self.cursor.fetchall()
    

adam = Adamastor(config)

print(" listar_clientes ".center(80, "."))
pprint(adam.listar_clientes())

print(" listar_clientes_da_cidade ".center(80, "."))
pprint(adam.listar_clientes_da_cidade("Lisboa"))

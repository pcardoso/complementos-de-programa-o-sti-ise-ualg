#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 10:11:25 2022

@author: pcardoso
"""

from person import Person
from color import Color
from engine import Engine


class Car:
    def __init__(self, owner, color, engine, brand, model, consumption, kms):
        assert isinstance(owner, Person)
        assert isinstance(color, Color)
        assert isinstance(engine, Engine)
        self.brand = brand
        self.model = model
        self.consumption = consumption
        self.kms = kms
        # TODO: "encapsulate" + addkms + ... see UML


if __name__ == "__main__":
    # test the class

    ze = Person()
    v8 = Engine()
    red = Color()

    car = Car(
        owner=ze,
        color=red,
        engine=v8,
        brand="UMM",
        model="Alter",
        consumption=8,
        kms=100000
    )
    print(car)

from car import Car
from person import Person
from color import Color
from engine import Engine
import pickle

list_of_persons = []
list_of_engines = []
list_of_colors = []
list_of_cars = []

def main():
    global list_of_colors, list_of_cars, list_of_engines, list_of_persons
    while True:
        print("Menu")
        print("p1 - New Person")
        print("p2 - List Persons")

        print("e1 - New Engine")
        print("e2 - List Engines")

        print("c1 - New Color")
        print("c2 - List Colors")

        print("v1 - New Car")
        print("v2 - List Cars")

        print("s - save lists")
        print("l - load lists")

        op = input("op? ")

        if op == "p1":
            # TODO: inputs
            new_person = Person()
            list_of_persons.append(new_person)
        elif op == "p2":
            print_list(list_of_persons)
        elif op == "e1":
            # TODO: inputs
            new_engine = Engine()
            list_of_engines.append(new_engine)
        elif op == "e2":
            print_list(list_of_engines)
        elif op == "c1":
            new_color = Color()
            list_of_colors.append(new_color)
        elif op == "c2":
            print_list(list_of_colors)
        elif op == "v1":
            # TODO: inputs
            new_vehicle()
        elif op == "v2":
           print_list(list_of_cars)
        elif op == "s":
            with open("colors_list.pkl", "wb") as f:
                pickle.dump(list_of_colors, f)
        elif op == "l":
            with open("colors_list.pkl", "rb") as f:
                list_of_colors = pickle.load(f)
            print_list(list_of_colors)


def print_list(list_of):
    # TODO: improve listing
    print([(i, p) for i, p in enumerate(list_of)])


def ask_id(msg, input_list):
    # TODO: validate returned id
    print_list(input_list)
    return int(input(msg))


def new_vehicle():
    person_id = ask_id("Person's id? ", list_of_persons)
    color_id = ask_id("Color's is? ", list_of_colors)
    engine_id = ask_id("Engine's id? ", list_of_engines)
    new_car = Car(
        owner=list_of_persons[person_id],
        color=list_of_colors[color_id],
        engine=list_of_engines[engine_id],
        brand="???",  # TODO
        model="???",  # TODO
        consumption=-1,  # TODO
        kms=-1  # TODO
    )
    list_of_cars.append(new_car)


main()


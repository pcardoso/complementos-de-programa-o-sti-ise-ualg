from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

# pip install ffpyplayer
import os
os.environ["KIVY_VIDEO"] = "ffpyplayer"

class Player(BoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        with open("lista_de_canais.txt", "r") as f:
            values = f.read().splitlines()

        self.ids.lista_de_canais.values = values
        self.ids.lista_de_canais.text = values[0]


class VideoPlayerApp(App):
    def build(self):
        return Player()

#on error try to install : pip install ffpyplayer
VideoPlayerApp().run()
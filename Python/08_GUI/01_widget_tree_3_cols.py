from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.label import Label




class LoginScreen(GridLayout):
    """ Classe que define a tela de login - versão 2
        Esta classe é uma versão da classe LoginScreen que utiliza o GridLayout
        com 3 colunas e adiciona um BoxLayout com 2 botões
    """
    def __init__(self, **kargs):
        super().__init__(**kargs)

        self.cols = 3

        # adiciona um widget do tipo Label na coluna 1
        self.add_widget(Label(text='Nome:'))

        # adiciona um widget do tipo TextInput na coluna 2
        self.add_widget(TextInput(multiline=False))

        # Cria um BoxLayout para os botões e adiciona-a à coluna 3
        box_layout = BoxLayout()
        box_layout.orientation = 'vertical'
        box_layout.add_widget(Button(text='ok'))
        box_layout.add_widget(Button(text='Cancelar'))
        self.add_widget(box_layout)


class MyApp(App):
    """ Classe principal da aplicação"""
    def build(self):
        return LoginScreen()


MyApp().run()
